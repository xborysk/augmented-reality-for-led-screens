import cv2
import numpy as np 
from ultralytics import YOLO
#from ultralytics.yolo.v8.detect.predict import DetectionPredictor

#https://github.com/ultralytics/ultralytics


def predict_camera():
    model = YOLO("yolov8s.pt")
    #model = YOLO("yolov8n.pt")
    results = model.predict(source="0", show=True)

    print(results)

def foo(): #https://docs.ultralytics.com/modes/predict/
    model = YOLO("yolov8s.pt")
    img = cv2.imread("./frames/white-frame2050.png")
    inputs = [img, img]  # list of numpy arrays
    results = model(inputs)  # list of Results objects

    for result in results:
        boxes = result.boxes  # Boxes object for bbox outputs
        masks = result.masks  # Masks object for segmentation masks outputs
        probs = result.probs  # Class probabilities for classification outputs
        print(boxes, masks, probs)

def predict_image_bbox(source): #https://docs.ultralytics.com/usage/cfg/#predict
    #model = YOLO("yolov8s.pt")
    model = YOLO("yolov8s.pt")
    #model.predict(source, save=True, imgsz=320, conf=0.5)
    #res = model.predict(source, save=True)
    #res = model(source, save=True, show=True) #augment, classes, retina_masks=True
    res = model(source, augment=True) #show, augment, classes, retina_masks=True
    res_plotted = res[0].plot(masks=True, probs=False, labels= False)
    cv2.imshow("result", res_plotted)

    print(res[0])
    masks = res[0].masks
    print("\n", "masks", masks)
    #cv2.imshow("masks", masks.xy)

    cv2.waitKey()


def predict_image_seg(source): #https://docs.ultralytics.com/usage/cfg/#predict
    frame = cv2.imread(source)
    h, w, _ = frame.shape
    model = YOLO("yolov8s-seg.pt") #model that does segmentation
    #model = YOLO("yolov8x-seg.pt") #model that does segmentation
    #model.predict(source, save=True, imgsz=320, conf=0.5)
    #res = model.predict(source, save=True)
    #res = model(source, save=True, show=True) #augment, classes, retina_masks=True
    res = model(source) #show, augment, classes, retina_masks=True
    res_plotted = res[0].plot(img=np.zeros((h,w,3), np.uint8), masks=True, labels=False, boxes=False)
    cv2.imshow("result", cv2.inRange(res_plotted, np.array([1,0,0]), np.array([255, 255, 255])))
    print(type(res_plotted))
    print(res_plotted.shape)

    cv2.waitKey()



#predict_image_bbox("./frames/white-frame2050.png")
predict_image_seg("./frames/white-frame2050.png")