import numpy as np
import time
import cv2


ARUCO_DICT = {
	"DICT_4X4_50": cv2.aruco.DICT_4X4_50,
	"DICT_4X4_100": cv2.aruco.DICT_4X4_100,
	"DICT_4X4_250": cv2.aruco.DICT_4X4_250,
	"DICT_4X4_1000": cv2.aruco.DICT_4X4_1000,
	"DICT_5X5_50": cv2.aruco.DICT_5X5_50,
	"DICT_5X5_100": cv2.aruco.DICT_5X5_100,
	"DICT_5X5_250": cv2.aruco.DICT_5X5_250,
	"DICT_5X5_1000": cv2.aruco.DICT_5X5_1000,
	"DICT_6X6_50": cv2.aruco.DICT_6X6_50,
	"DICT_6X6_100": cv2.aruco.DICT_6X6_100,
	"DICT_6X6_250": cv2.aruco.DICT_6X6_250,
	"DICT_6X6_1000": cv2.aruco.DICT_6X6_1000,
	"DICT_7X7_50": cv2.aruco.DICT_7X7_50,
	"DICT_7X7_100": cv2.aruco.DICT_7X7_100,
	"DICT_7X7_250": cv2.aruco.DICT_7X7_250,
	"DICT_7X7_1000": cv2.aruco.DICT_7X7_1000,
	"DICT_ARUCO_ORIGINAL": cv2.aruco.DICT_ARUCO_ORIGINAL,
	"DICT_APRILTAG_16h5": cv2.aruco.DICT_APRILTAG_16h5,
	"DICT_APRILTAG_25h9": cv2.aruco.DICT_APRILTAG_25h9,
	"DICT_APRILTAG_36h10": cv2.aruco.DICT_APRILTAG_36h10,
	"DICT_APRILTAG_36h11": cv2.aruco.DICT_APRILTAG_36h11
}


def aruco_display(corners, ids, rejected, image):
	if len(corners) > 0:
		
		c = (0, 255, 0)
		ids = ids.flatten()
		
		for (markerCorner, markerID) in zip(corners, ids):
			
			corners = markerCorner.reshape((4, 2))
			(topLeft, topRight, bottomRight, bottomLeft) = corners
			
			topRight = (int(topRight[0]), int(topRight[1]))
			bottomRight = (int(bottomRight[0]), int(bottomRight[1]))
			bottomLeft = (int(bottomLeft[0]), int(bottomLeft[1]))
			topLeft = (int(topLeft[0]), int(topLeft[1]))

			cv2.line(image, topLeft, topRight, c, 2)
			cv2.line(image, topRight, bottomRight, c, 2)
			cv2.line(image, bottomRight, bottomLeft, c, 2)
			cv2.line(image, bottomLeft, topLeft, c, 2)
			
			cX = int((topLeft[0] + bottomRight[0]) / 2.0)
			cY = int((topLeft[1] + bottomRight[1]) / 2.0)
			cv2.circle(image, (cX, cY), 4, c, -1)
			
			cv2.putText(image, str(markerID),(topLeft[0], topLeft[1] - 10), cv2.FONT_HERSHEY_SIMPLEX,
				0.5, c, 2)
			#print("[Inference] ArUco marker ID: {}".format(markerID))
			
	return image



def detect_video(input_file, output_file):

	#aruco_type = "DICT_7X7_250"
	aruco_type = "DICT_4X4_100"

	arucoDict = cv2.aruco.Dictionary_get(ARUCO_DICT[aruco_type])

	arucoParams = cv2.aruco.DetectorParameters_create()


	cap = cv2.VideoCapture(0)
	
	#h = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
	#w = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
	h = 720
	w = 1280
	fps = 50

	cap = cv2.VideoCapture(input_file)
	result = cv2.VideoWriter(output_file, cv2.VideoWriter_fourcc(*'MJPG'), fps, (w, h))
	#result = cv2.VideoWriter(output_file, cv2.VideoWriter_fourcc(*"mp4v"), fps, (w, h))
	


	cap.set(cv2.CAP_PROP_FRAME_WIDTH, w)
	cap.set(cv2.CAP_PROP_FRAME_HEIGHT, h)


	while cap.isOpened():
		
		ret, img = cap.read()

		corners, ids, rejected = cv2.aruco.detectMarkers(img, arucoDict, parameters=arucoParams)

		frame = aruco_display(corners, ids, rejected, img)

		cv2.imshow("Image", frame)
		result.write(frame)

		key = cv2.waitKey(1) & 0xFF
		if key == ord("q"):
			break

	cv2.destroyAllWindows()
	cap.release()
	result.release()


def detect_picture(img_file, out_file):
	aruco_type = "DICT_4X4_100"
	arucoDict = cv2.aruco.Dictionary_get(ARUCO_DICT[aruco_type])
	arucoParams = cv2.aruco.DetectorParameters_create()

	img = cv2.imread(img_file)
	#img = cv2.imread("./patterns/pattern01-white.png")
	#img = cv2.imread("./patterns/temp-p.png")
	#img = cv2.imread("./patterns/pattern01-yellow.png")

	corners, ids, rejected = cv2.aruco.detectMarkers(img, arucoDict, parameters=arucoParams)
	#qprint(corners, ids, rejected)


	frame = aruco_display(corners, ids, rejected, img)



	#cv2.imshow("Image", frame)
	cv2.imwrite(out_file, frame)
	cv2.waitKey()



#for i in range(74*50, 75*50):
#    detect_picture("./frames/white-frame" + str(i) + ".png", "./frames/white-detected-frame" + str(i) + ".png")

#detect_video("./videos-03-2023/clip1-magenta.mp4", "./videos-03-2023/magenta-detected.avi")
#detect_video("./videos-03-2023/clip3-cyan.mp4", "./videos-03-2023/cyan-detected.avi")
#detect_video("./videos-03-2023/clip4-green.mp4", "./videos-03-2023/green-detected.avi")
#detect_video("./videos-03-2023/clip5-yellow.mp4", "./videos-03-2023/yellow-detected.avi")

detect_video("./videos/002-video.mp4", "./videos/002-detected.avi")


