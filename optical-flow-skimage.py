import numpy as np 
import cv2
from skimage import registration


frame1 = cv2.imread('./frames/frame2500.png', cv2.IMREAD_GRAYSCALE)
frame2 = cv2.imread('./frames/frame2501.png', cv2.IMREAD_GRAYSCALE)

u, v = registration.optical_flow_tvl1(frame1, frame2)

#cv2.writeOpticalFlow('OpticalFlow.flo', flow)

norm = np.sqrt(u ** 2 + v ** 2)


# --- Quiver plot arguments

nvec = 20  # Number of vectors to be displayed along each image dimension
nl, nc = frame1.shape
step = max(nl//nvec, nc//nvec)

y, x = np.mgrid[:nl:step, :nc:step]
u_ = u[::step, ::step]
v_ = v[::step, ::step]

#TODO
#https://scikit-image.org/docs/stable/auto_examples/registration/plot_opticalflow.html#sphx-glr-auto-examples-registration-plot-opticalflow-py
