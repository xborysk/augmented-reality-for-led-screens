import numpy as np
import cv2



def draw_hsv(good_new, good_old, h, w):
	hsv = np.zeros((h, w, 3), np.uint8)
	#fx, fy = flow[:,:,0], flow[:,:,1]

	for i, (new, old) in enumerate(zip(good_new, good_old)):
		
		a, b = new #.ravel()
		c, d = old # .ravel()
		fx = c-a
		fy = d-b #TODO: takto nebo b-d?
		ang = np.arctan2(fy, fx) + np.pi
		v = np.sqrt(fx*fx+fy*fy)

		if a + 1 >= w or b + 1 >= h:
			continue
		
		hsv[round(b), round(a), 0] = ang*(180/np.pi/2) 
		hsv[round(b), round(a), 1] = 255
		hsv[round(b), round(a), 2] = np.maximum(v*4, 255)
	return cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)



#https://www.geeksforgeeks.org/python-opencv-optical-flow-with-lucas-kanade-method/
cap = cv2.VideoCapture("./videos/002-cut-mid.avi")

w, h = 1280, 720
fps = 50
result = cv2.VideoWriter("./videos/002-temp-LKflow.avi", cv2.VideoWriter_fourcc(*'MJPG'), fps, (w, h))


# params for corner detection
"""
feature_params = dict( maxCorners = 10,
					qualityLevel = 0.3,
					minDistance = 7,
					blockSize = 7 )
"""


# Parameters for lucas kanade optical flow
lk_params = dict( winSize = (6, 6),
				maxLevel = 2,
				criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT,
							10, 0.03))

# Create some random colors
color = np.random.randint(0, 255, (100, 3))

# Take first frame and find corners in it
ret, old_frame = cap.read()
old_gray = cv2.cvtColor(old_frame,
						cv2.COLOR_BGR2GRAY)
					
#p0 = cv2.goodFeaturesToTrack(old_gray, mask = None, **feature_params)



#p0 = np.array([[10, 20], [30, 40]])
#p0 = [[[10., 20.]], [[30., 40.]]]
p0 = np.array([[[float(x), float(y)]] for x in range(200, w-200, 3) for y in range(350, 550, 3)], dtype=np.float32)

#print(p0)
#print(p0.shape)

# Create a mask image for drawing purposes
mask = np.zeros_like(old_frame)

while(1):
	mask = np.zeros_like(old_frame)
	ret, frame = cap.read()
	frame_gray = cv2.cvtColor(frame,
							cv2.COLOR_BGR2GRAY)

	# calculate optical flow
	p1, st, err = cv2.calcOpticalFlowPyrLK(old_gray,
										frame_gray,
										p0, None,
										**lk_params)

	#if p1 is None:
		#continue
	# Select good points
	good_new = p1[st == 1]
	good_old = p0[st == 1]

	"""
	# draw the tracks
	for i, (new, old) in enumerate(zip(good_new,
									good_old)):

		a, b = new #.ravel()
		c, d = old # .ravel()
		#mask = cv2.line(mask, (round(a), round(b)), (100, 100), (0, 255, 0), 9)

		#mask = cv2.cvtColor(frame, cv2.COLOR_GRAY2BGR)
		mask = cv2.line(mask, (round(a), round(b)), (round(c), round(d)), color[i%100].tolist(), 2)
		frame = cv2.circle(frame, (round(a), round(b)), 5, color[i%100].tolist(), -1)
	"""


		
	#img = cv2.add(frame, mask)
	img = frame
	hsv = draw_hsv(good_new, good_old, h, w)
	cv2.imshow('frame', img)
	cv2.imshow('hsv', hsv)
	result.write(hsv)
	
	k = cv2.waitKey(5)
	if k == ord('q'):
		break


	# Updating Previous frame and points
	old_gray = frame_gray.copy()
	#p0 = cv2.goodFeaturesToTrack(old_gray, mask = None, **feature_params)
	#p0 = good_new.reshape(-1, 1, 2) #NOTE: comment this -> dense flow grid / uncomment this -> feature tracking

cv2.destroyAllWindows()
cap.release()



