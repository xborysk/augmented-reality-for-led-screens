import numpy as np
import cv2
import time



def draw_flow(img, flow, step=16):

    h, w = img.shape[:2]
    y, x = np.mgrid[step/2:h:step, step/2:w:step].reshape(2,-1).astype(int)
    fx, fy = flow[y,x].T

    lines = np.vstack([x, y, x-fx, y-fy]).T.reshape(-1, 2, 2)
    lines = np.int32(lines + 0.5)

    img_bgr = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    cv2.polylines(img_bgr, lines, 0, (0, 255, 0))

    for (x1, y1), (_x2, _y2) in lines:
        cv2.circle(img_bgr, (x1, y1), 1, (0, 255, 0), -1)
 
    return img_bgr


def draw_hsv(flow):

    h, w = flow.shape[:2]
    fx, fy = flow[:,:,0], flow[:,:,1]

    ang = np.arctan2(fy, fx) + np.pi
    v = np.sqrt(fx*fx+fy*fy)

    hsv = np.zeros((h, w, 3), np.uint8)
    hsv[...,0] = ang*(180/np.pi/2) #https://www.geeksforgeeks.org/what-is-three-dots-or-ellipsis-in-python3/
    hsv[...,1] = 255
    hsv[...,2] = np.minimum(v*4, 255)
    bgr = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)

    return bgr




#cap = cv2.VideoCapture(0) #from camera
cap = cv2.VideoCapture("./videos/002-cut-mid.avi") #from file

w, h = 1280, 720
fps = 50
result_flow = cv2.VideoWriter("./videos/002-temp-flow.avi", cv2.VideoWriter_fourcc(*'MJPG'), fps, (w, h))
result_hsv = cv2.VideoWriter("./videos/002-temp-hsv.avi", cv2.VideoWriter_fourcc(*'MJPG'), fps, (w, h))

suc, prev = cap.read()
prevgray = cv2.cvtColor(prev, cv2.COLOR_BGR2GRAY)



while cap.isOpened():

    suc, img = cap.read()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # start time to calculate FPS
    start = time.time()


    flow = cv2.calcOpticalFlowFarneback(prevgray, gray, None, 0.5, 3, 15, 3, 5, 1.2, 0)
    #flow = cv2.calcOpticalFlowFarneback(prevgray, gray, None, 0.5, 3, 15, 30, 5, 1.2, 0)
    
    prevgray = gray


    # End time
    end = time.time()
    # calculate the FPS for current frame detection
    fps = 1 / (end-start)

    #print(f"{fps:.2f} FPS")

    img_flow = draw_flow(gray, flow)
    img_hsv = draw_hsv(flow)
    #cv2.imshow('flow', img_flow)
    cv2.imshow('flow HSV', img_hsv)
    result_flow.write(img_flow)
    result_hsv.write(img_hsv)


    key = cv2.waitKey(5)
    if key == ord('q'):
        break


cap.release()
cv2.destroyAllWindows()