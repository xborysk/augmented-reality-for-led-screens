import cv2
import numpy as np

def cut_video(input_file, output_file, fst_frame, last_frame):
    cap = cv2.VideoCapture(input_file)
    #w, h = 1280, 720
    h = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    w = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    fps = int(cap.get(cv2.CAP_PROP_FPS))
    result = cv2.VideoWriter(output_file, cv2.VideoWriter_fourcc(*'MJPG'), fps, (w, h))

    cap.set(cv2.CAP_PROP_FRAME_WIDTH, w)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, h)
    
    #for _ in range(fst_frame):
     #   _, _ = cap.read() 

    cap.set(cv2.CAP_PROP_POS_FRAMES, fst_frame-1)
    for i in range (fst_frame, last_frame+1):
        if cap.isOpened():
            #print(i)
            ret, img = cap.read()
            #cv2.imshow("i", img)
            #cv2.waitKey()
            result.write(img)

    print("done")

    cv2.destroyAllWindows()
    cap.release()
    result.release()


def save_frame(input_file, output_file, frame_number):
    cap = cv2.VideoCapture(input_file)
    h = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    w = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, w)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, h)
    print(frame_number)
    cap.set(cv2.CAP_PROP_POS_FRAMES, frame_number-1)
    if cap.isOpened():
        _, img = cap.read()

        cv2.imshow("img", img)
        cv2.imwrite(output_file, img)
        cv2.waitKey(0)

    cv2.destroyAllWindows()
    cap.release()


def navigate_video_frames(video_path, frame_number=0):
    cap = cv2.VideoCapture(video_path)
    cap.set(cv2.CAP_PROP_POS_FRAMES, frame_number-1)
    ret, frame = cap.read()

    while ret:
        cv2.imshow("Video Frame", frame)
        key = cv2.waitKey(0)
        
        if key == ord("q"):  # Press Esc key to exit
            break
        elif key == ord("d"):  # Press D key to move to the next frame
            ret, frame = cap.read()
        elif key == ord("a"):  # Press A key to move to the previous frame
            cap.set(cv2.CAP_PROP_POS_FRAMES, cap.get(cv2.CAP_PROP_POS_FRAMES) - 2)
            ret, frame = cap.read()
        
    cap.release()
    cv2.destroyAllWindows()


def navigate_multiple_videos(video_paths):
    cap_list = []
    frame_list = []
    window_names = []


    # Open video capture for each video file
    for path in video_paths:
        cap = cv2.VideoCapture(path)
        cap_list.append(cap)
        ret, frame = cap.read()
        frame_list.append(frame)
        window_name = f"Video {len(cap_list)}"
        window_names.append(window_name)
        cv2.namedWindow(window_name)
        h = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
        w = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))

    while True:
        # Display frames from each video in separate windows
        for i in range(len(cap_list)):
            cv2.imshow(window_names[i], cv2.resize(frame_list[i], (w//2, h//2)))

        key = cv2.waitKey(0)

        if key == ord("q"):  # Press Esc key to exit
            break
        elif key == ord("d"):  # next frame for all videos
            for i in range(len(cap_list)):
                ret, frame_list[i] = cap_list[i].read()
        elif key == ord("a"):  # previous frame for all videos
            for i in range(len(cap_list)):
                cap_list[i].set(cv2.CAP_PROP_POS_FRAMES, cap_list[i].get(cv2.CAP_PROP_POS_FRAMES) - 2)
                ret, frame_list[i] = cap_list[i].read()

    # Release video captures and close windows
    for cap in cap_list:
        cap.release()
    cv2.destroyAllWindows()


def combine_four_videos(video_paths, output_width, output_height, output_filename, fps = 50):
    cap_list = []
    frame_list = []


    # Open video capture for each video file
    for path in video_paths:
        cap = cv2.VideoCapture(path)
        cap_list.append(cap)
        ret, frame = cap.read()
        frame = cv2.resize(frame, (output_width // 2, output_height // 2))  # Resize frames to common size
        frame_list.append(frame)

    # Create a VideoWriter object to save the final video
    fourcc = cv2.VideoWriter_fourcc(*"mp4v")  # Specify the codec
    output_video = cv2.VideoWriter(output_filename, fourcc, fps, (output_width, output_height))

    while True:
        combined_frame = np.zeros((output_height, output_width, 3), dtype=np.uint8)

        # Arrange the frames in quarters of the output frame
        combined_frame[:output_height // 2, :output_width // 2] = frame_list[0]
        combined_frame[:output_height // 2, output_width // 2:] = frame_list[1]
        combined_frame[output_height // 2:, :output_width // 2] = frame_list[2]
        combined_frame[output_height // 2:, output_width // 2:] = frame_list[3]

        # Add file names as text in the combined frame
        cv2.putText(combined_frame, "14 blocks", (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2, cv2.LINE_AA)
        cv2.putText(combined_frame, "7 blocks", (output_width // 2 + 10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2, cv2.LINE_AA)
        cv2.putText(combined_frame, "findHomography()", (10, output_height // 2 + 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2, cv2.LINE_AA)
        cv2.putText(combined_frame, "original (+markers)", (output_width // 2 + 10, output_height // 2 + 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2, cv2.LINE_AA)


        cv2.imshow("Combined Video", combined_frame)

        # Write the combined frame to the output video
        output_video.write(combined_frame)

        key = cv2.waitKey(1)
        if key == 27 or key == ord('q') or not ret:  # Press Esc / q key to exit
            break

        # Read the next frame for each video
        for i in range(len(cap_list)):
            ret, frame = cap_list[i].read()
            if ret:
                frame = cv2.resize(frame, (output_width // 2, output_height // 2))  # Resize frames to common size
                frame_list[i] = frame

    # Release video captures, close windows, and release the output video
    for cap in cap_list:
        cap.release()
    cv2.destroyAllWindows()
    output_video.release()



"""
#cut_video("./videos/video-002.mp4", "./videos/002-cut-beg.avi", 0*50, 11*50, 50)
#cut_video("./videos/video-002.mp4", "./videos/002-cut-mid.avi", 42*50, 52*50, 50)
#cut_video("./videos/wide.mp4", "./videos/wide-cut-beg.avi", 0*50, 10*50, 50)
#cut_video("./videos/wide.mp4", "./videos/wide-cut-mid.avi", 32*50, 40*50, 50)
#cut_video("./videos/closer.mp4", "./videos/closer-cut-beg.avi", 0*50, 10*50, 50)
#cut_video("./videos/closer.mp4", "./videos/closer-cut-mid.avi", 36*50, 42*50, 50)
#cut_video("./videos/closer.mp4", "./videos/closer-cut-color.avi", 19*50, 25*50, 50)
#cut_video("./videos/closer.mp4", "./videos/closer-cut-color.avi", 19*50, 25*50, 50)
"""
"""
#save_frame("./videos/wide.mp4", "./frames/wide2x50.png", 2*50)
#save_frame("./videos/wide.mp4", "./frames/wide32x50.png", 32*50)
#save_frame("./videos/closer.mp4", "./frames/closer1x50.png", 1*50)
#save_frame("./videos/closer.mp4", "./frames/closer19x50.png", 19*50)
#save_frame("./videos/closer.mp4", "./frames/closer36x50.png", 36*50)

#save_frame("./videos/002-cut-mid.avi", "./frames/temp.png", 2*50)
#save_frame("./videos/002-video.mp4", "./frames/temp.png", 46*50)
"""

"""
#NEW DATA
#for i in range(60*50, 61*50):
    #save_frame("./videos-03-2023/clip2-white.mp4", "./frames/white-frame" + str(i) + ".png", i)
    #save_frame("./videos-03-2023/temp.avi", "./outImg/temp.png", i)
#save_frame("./videos-03-2023/clip2-white.mp4", "./frames/temp.png", 41*50)
#save_frame("./videos-03-2023/cut-green.avi", "./frames/temp.png", 10*50)
#cut_video("./videos-03-2023/clip4-green.mp4", "./videos-03-2023/cut-green.avi", 11*50, 25*50, 50)
#cut_video("./videos-03-2023/clip2-white.mp4", "./videos-03-2023/cut-white2.avi", 0*50, 10*50, 50)
#cut_video("./videos-03-2023/clip3-cyan.mp4", "./videos-03-2023/cut-cyan.avi", 38*50, 46*50, 50)
#cut_video("./videos-03-2023/clip5-yellow.mp4", "./videos-03-2023/cut-yellow.avi", 48*50, 90*50, 50)
#cut_video("./videos-03-2023/clip2-white.mp4", "./videos-03-2023/cut-white3.avi", 85*50, 90*50, 50)
"""

save_frame("./videos-03-2023/clip3-cyan.mp4", "./frames/temp.png", 43*50+5)



#show_frames_simultaneously(["./videos-03-2023/yellow-diffkey.avi", "./videos-03-2023/yellow-chromakey.avi"])

#navigate_video_frames("./videos-03-2023/yellow-diffkey.avi")
#navigate_multiple_videos(["./videos-03-2023/yellow-diffkey.avi", "./videos-03-2023/yellow-chromakey.avi", "./videos-03-2023/yellow-segyolo.avi"])



#video_paths = ["./videos-03-2023/yellow-diffkey.avi", "./videos-03-2023/yellow-chromakey.avi", 
#               "./videos-03-2023/yellow-segyolo.avi", "./videos-03-2023/yellow-detected.avi"]

#video_paths = ["./videos-03-2023/green-diffkey.avi", "./videos-03-2023/green-chromakey.avi", 
#               "./videos-03-2023/green-segyolo.avi", "./videos-03-2023/green-detected.avi"]

#video_paths = ["./videos-03-2023/cyan-diffkey.avi", "./videos-03-2023/cyan-chromakey.avi", 
#               "./videos-03-2023/cyan-segyolo.avi", "./videos-03-2023/cyan-detected.avi"]

#video_paths = ["./videos-03-2023/magenta-diffkey.avi", "./videos-03-2023/magenta-chromakey.avi", 
#               "./videos-03-2023/magenta-segyolo.avi", "./videos-03-2023/magenta-detected.avi"]

#video_paths = ["./videos-03-2023/white-diffkey.avi", "./videos-03-2023/white-chromakey.avi", 
#               "./videos-03-2023/white-segyolo.avi", "./videos-03-2023/white-detected.avi"]

#output_width = 1920
#output_height = 1080


#video_paths = ["./videos/002-diffkey-7blocks.avi", "./videos/002-chromakey-7blocks.avi", 
#               "./videos/002-segyolo-7blocks.avi", "./videos/002-detected.avi"]

#video_paths = ["./videos/002-segyolo-14blocks.avi", "./videos/002-segyolo-7blocks.avi", 
#               "./videos/002-segyolo-0blocks.avi", "./videos/002-detected.avi"]

#output_width = 1280
#output_height = 720
#combine_four_videos(video_paths, output_width, output_height, "./videos/002-blocks-comparison.mp4")
