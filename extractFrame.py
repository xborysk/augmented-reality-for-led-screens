import cv2

def extract_frame(file_name, frameNumber = 0, output_file = "./outImg/temp.png"):
    cap = cv2.VideoCapture(file_name)

    # get total number of frames
    totalFrames = cap.get(cv2.CAP_PROP_FRAME_COUNT)

    # check for valid frame number
    if frameNumber >= 0 & frameNumber <= totalFrames:
        # set frame position
        cap.set(cv2.CAP_PROP_POS_FRAMES, frameNumber)
        ret, frame = cap.read()
        cv2.imwrite(output_file, frame)
        #cv2.imshow("frame", frame)
        #cv2.waitKey(0)
        cv2.destroyAllWindows()
        return frame

    """
    while True:
        ret, frame = cap.read()
        cv2.imshow("Video", frame)
        if cv2.waitKey(20) & 0xFF == ord('q'):
            break
    """


for i in range(2500, 2550):
    extract_frame('./videos/002-video.mp4', i, "./frames/frame" + str(i) + ".png")