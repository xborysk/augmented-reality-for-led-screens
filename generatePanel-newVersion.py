import numpy as np
import cv2
import os
#FOR NEW VERSION OF openCV


ARUCO_DICT = {
	"DICT_4X4_50": cv2.aruco.DICT_4X4_50,
	"DICT_4X4_100": cv2.aruco.DICT_4X4_100,
	"DICT_4X4_250": cv2.aruco.DICT_4X4_250,
	"DICT_4X4_1000": cv2.aruco.DICT_4X4_1000,
	"DICT_5X5_50": cv2.aruco.DICT_5X5_50,
	"DICT_5X5_100": cv2.aruco.DICT_5X5_100,
	"DICT_5X5_250": cv2.aruco.DICT_5X5_250,
	"DICT_5X5_1000": cv2.aruco.DICT_5X5_1000,
	"DICT_6X6_50": cv2.aruco.DICT_6X6_50,
	"DICT_6X6_100": cv2.aruco.DICT_6X6_100,
	"DICT_6X6_250": cv2.aruco.DICT_6X6_250,
	"DICT_6X6_1000": cv2.aruco.DICT_6X6_1000,
	"DICT_7X7_50": cv2.aruco.DICT_7X7_50,
	"DICT_7X7_100": cv2.aruco.DICT_7X7_100,
	"DICT_7X7_250": cv2.aruco.DICT_7X7_250,
	"DICT_7X7_1000": cv2.aruco.DICT_7X7_1000,
	"DICT_ARUCO_ORIGINAL": cv2.aruco.DICT_ARUCO_ORIGINAL,
	"DICT_APRILTAG_16h5": cv2.aruco.DICT_APRILTAG_16h5,
	"DICT_APRILTAG_25h9": cv2.aruco.DICT_APRILTAG_25h9,
	"DICT_APRILTAG_36h10": cv2.aruco.DICT_APRILTAG_36h10,
	"DICT_APRILTAG_36h11": cv2.aruco.DICT_APRILTAG_36h11
}

#TODO one universal function
#TODO scaling
"""
marker_dict_size only used for dictionary
marker_size is number of pixels of the printed picture of the marker
"""
def generate_panel(num_squares, marker_size, out_file, first_id=0, panel_height = 128, panel_width = 6720, separator_size = 5):
    aruco_type = "DICT_{num}X{num}_250".format(num=num_squares)
    aruco_dict = cv2.aruco.getPredefinedDictionary(ARUCO_DICT[aruco_type])

    #panel details
    #panel_width = 6720
    #panel_height = 80
    border_value = 255
    #separator_size = 5
    

    #generating
    marker = np.zeros((marker_size, marker_size), dtype="uint8")
    #cv2.aruco.drawMarker(aruco_dict, 0, marker_size, marker, 1)
    panel = np.full((panel_height, separator_size), border_value, dtype="uint8")


    for i in range(0, (panel_width - separator_size) // (marker_size + separator_size)):
        #cv2.aruco.drawMarker(aruco_dict, first_id+i, marker_size, marker, 1)
        cv2.aruco.generateImageMarker(aruco_dict, first_id+i, marker_size, marker, 1)
        panel = np.append(panel, add_top_bottom_border(marker, marker_size, panel_height, border_value), axis=1)
        panel = np.append(panel, np.full((panel_height, separator_size), border_value, dtype="uint8"), axis=1)

    panel = np.append(panel, np.full((panel_height, (panel_width - separator_size) % (marker_size+separator_size)), border_value, dtype="uint8"), axis=1)




    #save and show
    cv2.imshow("ArUCo", panel)
    cv2.imwrite(out_file, panel)

    cv2.waitKey(0)
    cv2.destroyAllWindows()




def add_top_bottom_border(marker, marker_size, new_height, fill_value):
    strip_height = (new_height - marker_size) // 2
    if strip_height == 0:
        return marker

    #TODO add input check for negative strip_height

    top_down_border = np.full((strip_height, marker_size), fill_value, dtype="uint8")
    new_marker = top_down_border
    new_marker = np.append(new_marker, marker, axis=0)
    new_marker = np.append(new_marker, top_down_border, axis=0)
    return new_marker


#TODO clean this function
def generate_twoline_panel(num_squares,  marker_size, out_file):
    panel_height = 128
    file_path = './generatedArucoMarkerPanels/'
    generate_panel(num_squares, marker_size, file_path + 'temp1.png', 0, panel_height//2)
    generate_panel(num_squares, marker_size, file_path + 'temp2.png', 110, panel_height//2)
    temp1 = cv2.imread(file_path + 'temp1.png')
    temp2 = cv2.imread(file_path + 'temp2.png')
    result = np.append(temp1, temp2, axis=0)

    #save and show
    cv2.imshow("ArUCo", result)
    cv2.imwrite(out_file, result)

    cv2.waitKey(0)
    cv2.destroyAllWindows()

def generate_twodicts_panel(num1_squares, num2_squares, marker1_size, out_file, separator=5):
    file_path = './generatedArucoMarkerPanels/'
    marker2_size = 128 - marker1_size - 2*8 # 2* (top + bottom borders)
    generate_panel(num1_squares, marker1_size, file_path + 'temp1.png', 0, marker1_size + 8, separator)
    generate_panel(num2_squares, marker2_size, file_path + 'temp2.png', 0, marker2_size + 8, separator)
    temp1 = cv2.imread(file_path + 'temp1.png')
    temp2 = cv2.imread(file_path + 'temp2.png')
    result = np.append(temp1, temp2, axis=0)

    #save and show
    cv2.imshow("ArUCo", result)
    cv2.imwrite(out_file, result)

    cv2.waitKey(0)
    cv2.destroyAllWindows()



def main():
    #file_path = './generatedArucoMarkerPanels/'
    #generate_panel(4, 90, file_path + 'panel1.png')
    #generate_panel(4, 120, file_path + 'panel2.png')
    #generate_panel(5, 90, file_path + 'panel3.png') 
    #generate_panel(5, 120, file_path + 'panel4.png')
    #generate_panel(7, 90, file_path + 'panel5.png')
    #generate_panel(7, 120, file_path + 'panel6.png')

    #TWO DICT, TWO ROWS
    """
    generate_twodicts_panel(4, 7, 56, file_path + 'tworowsPanel1.png')
    generate_twodicts_panel(4, 7, 64, file_path + 'tworowsPanel2.png')
    generate_twodicts_panel(4, 7, 80, file_path + 'tworowsPanel3.png') # not able to detect from png but able from camera
    """

    #ONE DICT, TWO ROWS
    """
    generate_twoline_panel(4, 56, file_path + 'tworowsPanel4.png')
    generate_twoline_panel(5, 56, file_path + 'tworowsPanel5.png')
    generate_twoline_panel(7, 56, file_path + 'tworowsPanel6.png')
    """


    """
    aruco_dict = cv2.aruco.Dictionary_get(ARUCO_DICT["DICT_5X5_1000"])
    marker = np.zeros((200, 200), dtype="uint8")
    cv2.aruco.drawMarker(aruco_dict, 0, 200, marker, 1)
    cv2.imshow("marker", marker)
    cv2.waitKey(0)
    """


    #generate_panel(4, 70, './patterns/pattern02.png', separator_size=20)
    #generate_panel(4, 70, './patterns/pattern01.png', separator_size=25)
    #generate_panel(4, 50, './patterns/pattern05.png', separator_size=25)
    #generate_panel(4, 50, './patterns/pattern06.png', separator_size=30)
    generate_panel(4, 70, './patterns/temp.png', separator_size=25)

if __name__ == "__main__":
    main()