
from skimage import data
from skimage import exposure
from skimage.exposure import match_histograms
import pixellib
from pixellib.instance import instance_segmentation
#from pixellib.semantic import semantic_segmentation



#PIXELLIB: INSTANCE SEGMENTATION
segment_instance = instance_segmentation()
segment_instance.load_model("mask_rcnn_coco.h5") #from https://github.com/matterport/Mask_RCNN/releases/tag/v2.0
target_classes = segment_instance.select_target_classes(person=True)
#segment_instance.process_video("./videos-03-2023/cut-white2.avi", frames_per_second=50, output_video_name="./videos-03-2023/temp-segmentation.avi")


#segment_instance.segmentImage("./frames/06.png", show_bboxes=True, text_thickness=1 output_image_name="./outImg/temp-instance.png", segment_target_classes= target_classes)
#segment_instance.segmentImage("./frames/white-frame2050.png", show_bboxes=True, text_thickness=1, output_image_name="./outImg/temp-instance.png", segment_target_classes= target_classes )
#segment_instance.segmentImage("./frames/white-frame2050.png", output_image_name="./outImg/temp-instance.png")
#segment_instance.segmentImage("./frames/downloaded.jpeg", show_bboxes=True, text_thickness=1, output_image_name="./outImg/temp-instance.png", segment_target_classes= target_classes )
#segment_instance.segmentImage("./frames/sample2.jpg", show_bboxes=True, output_image_name="./outImg/temp-instance.png", segment_target_classes= target_classes )
segment_instance.segmentImage("./frames/sample2.jpg", show_bboxes=True, output_image_name="./outImg/temp-instance.png")








#PIXELLIB: SEMANTIC SEGMENTATION
#segment_semantic = semantic_segmentation()
#segment_semantic.load_pascalvoc_model("deeplabv3_xception_tf_dim_ordering_tf_kernels.h5") 
#segment_semantic.segmentAsPascalvoc("./frames/white-frame2050.png", output_image_name = "./outImg/temp-semantic.png")