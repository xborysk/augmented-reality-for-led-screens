import numpy as np
import time
import cv2 
import time
import sys
import random
import math
from enum import Enum
import matplotlib.pyplot as plt
from ultralytics import YOLO

#FOR OLDER VERSION OF OPENCV
#pip install opencv-contrib-python==4.6.0.66
#to get the right ArUco library version


ARUCO_DICT = {
	"DICT_4X4_50": cv2.aruco.DICT_4X4_50,
	"DICT_4X4_100": cv2.aruco.DICT_4X4_100,
	"DICT_4X4_250": cv2.aruco.DICT_4X4_250,
	"DICT_4X4_1000": cv2.aruco.DICT_4X4_1000,
	"DICT_5X5_50": cv2.aruco.DICT_5X5_50,
	"DICT_5X5_100": cv2.aruco.DICT_5X5_100,
	"DICT_5X5_250": cv2.aruco.DICT_5X5_250,
	"DICT_5X5_1000": cv2.aruco.DICT_5X5_1000,
	"DICT_6X6_50": cv2.aruco.DICT_6X6_50,
	"DICT_6X6_100": cv2.aruco.DICT_6X6_100,
	"DICT_6X6_250": cv2.aruco.DICT_6X6_250,
	"DICT_6X6_1000": cv2.aruco.DICT_6X6_1000,
	"DICT_7X7_50": cv2.aruco.DICT_7X7_50,
	"DICT_7X7_100": cv2.aruco.DICT_7X7_100,
	"DICT_7X7_250": cv2.aruco.DICT_7X7_250,
	"DICT_7X7_1000": cv2.aruco.DICT_7X7_1000,
	"DICT_ARUCO_ORIGINAL": cv2.aruco.DICT_ARUCO_ORIGINAL,
	"DICT_APRILTAG_16h5": cv2.aruco.DICT_APRILTAG_16h5,
	"DICT_APRILTAG_25h9": cv2.aruco.DICT_APRILTAG_25h9,
	"DICT_APRILTAG_36h10": cv2.aruco.DICT_APRILTAG_36h10,
	"DICT_APRILTAG_36h11": cv2.aruco.DICT_APRILTAG_36h11
}

class OcclusionMethod(Enum):
    DIFF_KEY = 1
    CHROMA_KEY = 2
    SEG_YOLO = 3



################### DETECTION ###################


def aruco_detect(img, aruco_type = "DICT_4X4_250"): 
    """Detects ArUco markers in the img.
    
    Args:
        img: numpy.ndarray with the loaded image
        aruco_type (string): key for ARUCO_DICT variable

    Returns:
        Tuple: all detected corners and IDs;
        each element in corners holds 4 coordinates (the corners) of the detected marker
    """
    arucoDict = cv2.aruco.Dictionary_get(ARUCO_DICT[aruco_type]) #different dict size -> different speed	
    arucoParams = cv2.aruco.DetectorParameters_create()
    corners, ids, _ = cv2.aruco.detectMarkers(img, arucoDict, parameters=arucoParams)
    return (corners, ids) #not zipped
    #NOTE: possible enhancement:  cv2.aruco.refineDetectedMarkers()


def detect_in_pattern(pattern):
    """Detects ArUco markers in the pattern and returns them in a dictionary.

    Arg:
        pattern (numpy.ndarray): An image of a pattern containing ArUco markers.
          IDs in the pattern image should be sorted from left to right

    Returns:
        Dict: A dictionary containing the detected ArUco markers in the pattern image.
        Keys are marker IDs (integers), and values are arrays of shape (4, 2) representing the corner points of each marker.
    """
    corners, ids = aruco_detect(pattern)
    if ids is None:
        print("WARNING WRONG PATTERN - no aruco detected in the pattern")
        return []
    ids = ids.flatten()
    pattern_all_markers = {}
    for (id, corner) in zip(ids, corners):
        pattern_all_markers[id] = corner # dictionary
    return pattern_all_markers


def detect_in_frame(frame):
    """Detects ArUco markers in the frame and returns them in a sorted list.

    Arg:
        frame (numpy.ndarray): Frame image in BGR format.

    Returns:
        List[Tuples]: A sorted list of tuples where the first element is the marker ID (integers)
        and the second element is an array of shape (4, 2) representing the corner points of each marker.
    """

    corners, ids = aruco_detect(frame)
    if ids is None:
        return []
    ids = ids.flatten()
    curr_frame_markers = list(zip(ids, corners))

    curr_frame_markers = sorted(curr_frame_markers, key=lambda x: x[0])
    return curr_frame_markers



################### MARKERS STRUCTURE ###################


def get_arucoID_by_index(markers_list, index):
    return markers_list[index][0]


def get_corners_by_index(markers_list, index):
    return markers_list[index][1][0].copy()


def get_corners_by_id(markers_dict, id):
    return markers_dict[id][0].copy()
    

def get_top_bottom_line_points(curr_frame_markers, pattern_all_markers): 
    """Creates structure for corners of the detected markers.

    Matches points (corners of detected markers) from frame to points in pattern
    and divides them into two lists of tuples: (frame_point, pattern_point),
    where first list contains all top left and top right corners
    and second list contains all bottom left and bottom right corners.

    These structures help to secure computation of perspective transformation (easy colinearity check).

    Args:
        curr_frame_markers (list): List of current frame markers.
        pattern_all_markers (dict): Dictionary of pattern markers.

    Returns:
        tuple: Tuple containing list of all top corners from the frame 
            matched to corresponding pattern corners, and list o all bottom (analogically)

    NOTE: markers detected only in the pattern are not in the resulting lists
    NOTE: final lists hold no information about the orginal ID of the marker
    NOTE: lists are sorted by x-coord
    
    """
    if(len(curr_frame_markers) == 0):
        print("WARNING get_top_bottom_line_points: 'no features to match'")

    top_line_points = []
    bottom_line_points = []
    for (markerID, [marker_corners]) in curr_frame_markers:
        if markerID not in pattern_all_markers:
            continue
        frame_top_left, frame_top_right, frame_bottom_right, frame_bottom_left = marker_corners
        pattern_top_left, pattern_top_right, pattern_bottom_right, pattern_bottom_left = get_corners_by_id(pattern_all_markers, markerID)

        top_line_points.append((frame_top_left, pattern_top_left))
        top_line_points.append((frame_top_right, pattern_top_right))

        bottom_line_points.append((frame_bottom_left, pattern_bottom_left))
        bottom_line_points.append((frame_bottom_right, pattern_bottom_right))

    top_line_points = sorted(top_line_points, key=lambda x: x[0][0]) # x coord of first element of the tuple
    bottom_line_points = sorted(bottom_line_points, key=lambda x: x[0][0])
    #NOTE: if we additionaly sort the points by x-coord here 
    # and then in BLOCK PROCCESING we look only at these two lists, but no more at the IDs,
    # we don't need to enforce the ordering condition in the original pattern
    return (top_line_points, bottom_line_points)



################### WARPING ###################


def warp_overlay(frame, overlay, curr_frame_markers, pattern_all_markers, n_blocks = 0):
    """Warps an overlay image onto an empty frame image based on detected markers.

    Args:
        frame (numpy.ndarray): Frame image in BGR format.
        overlay (numpy.ndarray): Overlay image in BGR format.
        curr_frame_markers (List): List of detected markers in the current frame.
        pattern_all_markers (Dist): Dict of markers in the pattern.
        n_blocks (int, optional): Number of blocks to divide the pattern into. 
            Default is to 0, which means, that all the points are used with findHomography().

    Returns:
        numpy.ndarray: Warped overlay image with the same shape as the frame image.
    """
    h, w, _ = frame.shape
    _, overlay_w, _ = overlay.shape
    result = np.zeros((h,w,3), np.uint8) #blank image

    top_line_points, bottom_line_points = get_top_bottom_line_points(curr_frame_markers, pattern_all_markers)

    #OPTIONAL
    if(len(top_line_points) <= 3 or len(top_line_points) != len(bottom_line_points)): 
        #print("WARNING warp_overlay(): wrong num of markers in the frame")
        return frame

    #PREPROCCESING
    #in case a pixel in overlay is full black, it will not be drawn
    pure_black_area = cv2.inRange(overlay, (0, 0, 0), (0, 0, 0))
    overlay = cv2.add(overlay, 1, overlay, mask=pure_black_area) # this ads one to all pixels that were set to (0, 0, 0)
    #TODO: this only needs to be done twice during the video (once for the advertisement, once for the pattern), or when the ad/pattern changes

    n_blocks = min(n_blocks, len(curr_frame_markers)-1) #the smallest block consist of one marker

    # ONE BLOCK + findHomography from all points
    if(n_blocks == 0):
        frame_points, pattern_points = zip(*(top_line_points + bottom_line_points))
        matrix, _ = cv2.findHomography(np.array(pattern_points), np.array(frame_points), cv2.RANSAC, 5.0)
        return cv2.warpPerspective(overlay, matrix, (w,h))


    # N BLOCKS + findHomography from only 4 corner points
    step = len(top_line_points) // n_blocks
    for i in range(0, n_blocks):
        beg = i*step 
        end = min((i+1)*step, len(top_line_points)-1)

        frame_points = [top_line_points[beg][0].copy(), top_line_points[end][0].copy(), bottom_line_points[end][0].copy(), bottom_line_points[beg][0].copy()] 
        pattern_points = [top_line_points[beg][1].copy(), top_line_points[end][1].copy(), bottom_line_points[end][1].copy(), bottom_line_points[beg][1].copy()]
        
        #drawUsedFeatures(frame, frame_points, './outImg/temp-corners1.png')
        #drawUsedFeatures(pattern, pattern_points, './outImg/temp-corners2.png')


        #PATTERN CROPING AND COORDS ADJUSTING

        left_crop = round(pattern_points[0][0]) #x value of the most left point
        right_crop = round(pattern_points[1][0]) #x value of the most right point
        #NOTE: the order of the points in pattern_points matter

        if(left_crop >= right_crop): # not enough points to calculate the perspective transformation
            continue 
        
        if(i == 0): #first block
            left_crop=0

        if(i == n_blocks-1): #last block
            right_crop = overlay_w

        adjusted_pattern_points = [(round(x - left_crop), round(y)) for x, y in pattern_points]
        cropped_overlay = overlay[:, left_crop:right_crop]
        
        matrix, _ = cv2.findHomography(np.array(adjusted_pattern_points), np.array(frame_points))
        #NOTE: block processing -> proccesing more than 4 pair points may cause discontinuities between blocks

        curr_warped = cv2.warpPerspective(cropped_overlay, matrix, (w,h))
        result = cv2.add(result, curr_warped)


    return result


def blend_images(img1, img2):
    """Blends two images together.

    img2 is pasted over img1.

    Args:
        img1 (numpy.ndarray): First input image.
        img2 (numpy.ndarray): Second input image.

    Returns:
        numpy.ndarray: Blended image.

    """
    mask = cv2.inRange(img2, 0, 0)
    temp1 = cv2.bitwise_and(img1, img1, mask = mask)
    temp2 = cv2.bitwise_and(img2, img2, mask = cv2.bitwise_not(mask))
    return cv2.add(temp1, temp2)


def get_warped_overlaid_frame(frame, overlay, curr_frame_markers, pattern_all_markers, n_blocks=0):
    """Warps an overlay image onto the frame image based on detected markers.

    Args:
        frame (numpy.ndarray): Frame image in BGR format.
        overlay (numpy.ndarray): Overlay image in BGR format.
        curr_frame_markers (List): List of detected markers in the current frame.
        pattern_all_markers (Dist): Dict of markers in the pattern.
        n_blocks (int, optional): Number of blocks to divide the pattern into. 
            Defaults to 1.

    Returns:
        numpy.ndarray: Warped overlay onto frame image with the same shape as the frame image.
    """
    warped_overlay = warp_overlay(frame, overlay, curr_frame_markers, pattern_all_markers, n_blocks)
    return blend_images(frame, warped_overlay)


def alpha_blending(background, foreground, alpha): #https://learnopencv.com/alpha-blending-using-opencv-cpp-python/
    """Performs alpha blending of a foreground image onto a background image.

    Args:
        background (numpy.ndarray): The background image as a numpy array.
        foreground (numpy.ndarray): The foreground image as a numpy array.
        alpha (numpy.ndarray): The alpha mask as a numpy array.

    Returns:
        numpy.ndarray: The blended image.

    """
    #Convert uint8 to float
    foreground = foreground.astype(float)
    background = background.astype(float)
    
    # Normalize the alpha mask to keep intensity between 0 and 1
    alpha = alpha.astype(float)/255

    foreground = cv2.multiply(alpha, foreground)
    background = cv2.multiply(1.0 - alpha, background) #TODO: mrknout na blue channel

    outImage = cv2.add(foreground, background)

    return cv2.convertScaleAbs(outImage) #NOTE: not sure if this is the fastest way but it converts it back to uint8



################### OCCLUSIONS ###################


def get_diff_key_mask(frame, overlaid):
    """Generates a difference key mask between the frame and overlaid image.

    Args:
        frame (numpy.ndarray): Input frame.
        overlaid (numpy.ndarray): Overlay image.

    Returns:
        numpy.ndarray: Difference key mask.

    """

    # DIFFERENCE IMAGE
    absdiff = cv2.absdiff(frame, overlaid)
    hsvAbsDiff = cv2.cvtColor(absdiff, cv2.COLOR_BGR2HSV) # H: 0 a 179, S: 0 a 255, V: 0 a 255

    """
    # RANGES FOR VIDEOS FROM CAMERA A
    # BLACKS
    lower_blue = np.array([105,0,0])
    upper_blue = np.array([135,255,255])

    # WHITES
    lower_red = np.array([0,1,0])
    upper_red = np.array([10,255,255])
    """
    
    
    # RANGES FOR VIDEOS FROM CAMERA B
    # BLACKS
    lower_blue = np.array([110,0,0])
    upper_blue = np.array([130,255,255])

    # WHITES
    lower_yellow = np.array([10,0,0])
    upper_yellow = np.array([30,255,100])
    

    # OPTIONAL WHITES (based on saturation) - NOTE: not that accurate
    #lower_red = np.array([0,250,0])
    #upper_red = np.array([180,255,255])


    # THRESHOLDING
    mask1 = cv2.inRange(hsvAbsDiff, lower_blue, upper_blue)  #threshold the HSV image to get only blue colors
    #mask2 = cv2.inRange(hsvAbsDiff, lower_red, upper_red)  #threshold the HSV image to get only red colors
    mask2 = cv2.inRange(hsvAbsDiff, lower_yellow, upper_yellow)  #threshold the HSV image to get only red colors

    mask = cv2.bitwise_or(mask1, mask2)
    return mask

    """
    cv2.imshow("absdiff", absdiff)
    cv2.imshow("diff1", diff1)
    cv2.imshow("diff2", diff2)
    cv2.imshow("mask1", mask1)
    cv2.imshow("mask2", mask2)
    cv2.imshow("mask", mask)
    cv2.waitKey()
    """


def get_chroma_key_mask(frame, curr_frame_markers):
    """Generates a mask of all pixels in the frame of colors similar to LED board.

    Args:
        frame (numpy.ndarray): Input frame.
        curr_frame_markers (list): List of current frame markers.
        coarse_mask (numpy.ndarray): A coarse mask to be applied to narrow down the final selection.

    Returns:
        numpy.ndarray: Generated mask.

    """
    mask = np.zeros(frame.shape[:2], dtype="uint8")
    corners_fst_marker = get_corners_by_index(curr_frame_markers, 0)
    corners_last_marker = get_corners_by_index(curr_frame_markers, len(curr_frame_markers)-1)

    bounding_boxes = [get_bounding_box(corners_fst_marker), 
                      get_bounding_box(corners_last_marker),
                      get_marker_top_line_box(corners_fst_marker), 
                      get_marker_top_line_box(corners_last_marker)]
    #NOTE: there can be other options that could bring better stability,
    #  like for loop over all detected markers

    
    for bbox in bounding_boxes:
        (x_min, x_max, y_min, y_max) = bbox
        colors = get_random_pixels_colors(frame[y_min:y_max, x_min:x_max], num_pixels=20)
        mask = cv2.bitwise_or(mask, create_color_mask(frame, colors))

    return mask


def segment_image_YOLO(frame, model= YOLO("yolov8m-seg.pt"), classes=None): #https://docs.ultralytics.com/usage/cfg/#predict
    """Performs image segmentation using the YOLO model.

    Args:
        frame (numpy.ndarray): Input frame.
        model (YOLO): YOLO model for segmentation.
        classes (list): List of classes to be segmented. Set to None for all classes.

    Returns:
        numpy.ndarray: Segmented mask.

    """
    h, w, _ = frame.shape
    res = model(frame, classes=classes) #class: 0 --> human, None --> all classes
    res_plotted = res[0].plot(img=np.zeros((h,w,3), np.uint8), masks=True, labels=False, boxes=False)
    mask = cv2.bitwise_not(cv2.inRange(res_plotted, 0, 0)) #make it binary

    return mask


def get_bounding_box(points):
    """Calculates the bounding box coordinates of a set of points.

    Args:
        points (list): List of (x, y) coordinate points.

    Returns:
        tuple: Tuple containing the minimum and maximum x and y coordinates of the bounding box.

    """
    min_x = round(min(points, key=lambda p: p[0])[0])
    min_y = round(min(points, key=lambda p: p[1])[1])
    max_x = round(max(points, key=lambda p: p[0])[0])
    max_y = round(max(points, key=lambda p: p[1])[1])
    return min_x, max_x, min_y, max_y


def get_inner_box(quad_corners, err = 5):
    """Calculates the inner box coordinates (x_min, x_max, y_min, y_max) of a quad.
        Ensures that all picked area is inside the quad (quad must be convex).
    
    Args:
        quad_corners (list): List of (x, y) coordinate points of the quad corners.

    Returns:
        tuple: Tuple containing the inner bounding box coordinates (x_min, x_max, y_min, y_max).

    """
    x = sorted(list(map(lambda xy: round(xy[0]), quad_corners)))
    y = sorted(list(map(lambda xy: round(xy[1]), quad_corners)))

    min_x = x[1] + err
    max_x = x[2] - err
    min_y = y[1] + err
    max_y = y[2] - err

    return (min_x, max_x, min_y, max_y)


def get_marker_top_line_box(quad_corners):
    """Calculates a box for the area around top line of a marker.

    Args:
        quad_corners (list of tuples): List of four corner points of the marker quad.

    Returns:
        tuple: The bounding box coordinates in the form (x_min, x_max, y_min, y_max).

    """
    x = sorted(list(map(lambda xy: round(xy[0]), quad_corners)))
    y = sorted(list(map(lambda xy: round(xy[1]), quad_corners)))

    err = 5
    min_x = x[0] - err
    max_x = x[3] + err
    min_y = y[0] - err
    max_y = y[0]

    return (min_x, max_x, min_y, max_y)


def get_random_pixels_colors(image, num_pixels=10):
    """Randomly selects pixels from an image and returns array of their colors.

    Parameters:
        image: NumPy array representing the image
        num_pixels: Number of pixels to select (default: 10)

    Returns:
        colors: NumPy array of shape (num_pixels, 3) containing
              the colors of the randomly selected pixels in BGR order
    """
    height, width = image.shape[:2]

    if height == 0 or width == 0:
        return []
    
    coords = [(random.randint(0, width - 1), random.randint(0, height - 1))
              for _ in range(num_pixels)]
    colors = np.array([image[y, x] for x, y in coords])
    return colors


def create_color_mask(image, colors, err = np.array([10, 10, 10])): #TODO: err -> do hsv a pohrat si s hodnotami
    """Creates a binary mask from an image and array of colors,
        where white means that the pixel color is in the array.
    
    Args:
        image: NumPy array representing the image
        colors: List or NumPy array of colors to include in the mask
    
    Returns:
        mask: Binary mask with the same shape as the input image,
            where white pixels indicate colors in the input array
    """
    result = np.zeros(image.shape[:2], dtype=np.uint8)
     
    for color in colors:
        color_mask = cv2.inRange(image, color-err, color+err)
        result = cv2.bitwise_or(result, color_mask) 

    """
    cv2.imshow("chromakey", result)
    cv2.waitKey()
    """
    return result



################### ALGORITHM ###################


def process_frame_with_occlusions(frame, pattern, overlay, curr_frame_markers, pattern_all_markers, 
                        method = OcclusionMethod.SEG_YOLO, n_blocks = 0):
    """Processes a frame, handle occlusions and overlay it with new content.

    Args:
        frame (numpy.ndarray): Input frame to process.
        pattern (numpy.ndarray): Pattern image.
        overlay (numpy.ndarray): Overlay image.
        curr_frame_markers (list): List of current frame markers.
        pattern_all_markers (dict): Dictionary of pattern markers.
        method (OcclusionMethod): Method to handle occlusions (default: OcclusionMethod.SEG_YOLO).
        n_blocks (int): Number of blocks for warping (default: 1).

    Returns:
        numpy.ndarray: Processed frame with new content.

    """
    
    if len(curr_frame_markers) == 0: #NOTE: stronger condition also in warp_overlay()
        return frame
    
    # WARPING
    warped_overlay = warp_overlay(frame, overlay, curr_frame_markers, pattern_all_markers, n_blocks=n_blocks)

    # COARSE MASK
    coarse_mask = cv2.inRange(warped_overlay, 0, 0) #so it's binary
    coarse_mask = cv2.bitwise_not(coarse_mask)

    # OCCLUSIONS
    match method:
        case OcclusionMethod.DIFF_KEY:
            warped_pattern = warp_overlay(frame, pattern, curr_frame_markers, pattern_all_markers, n_blocks=n_blocks)
            #warped_pattern = cv2.bitwise_and(warped_pattern, warped_pattern, mask = coarse_mask)
            mask = get_diff_key_mask(frame, blend_images(frame, warped_pattern))
        case OcclusionMethod.CHROMA_KEY:
            mask = get_chroma_key_mask(frame, curr_frame_markers)
            mask = cv2.bitwise_and(mask, coarse_mask)
        case OcclusionMethod.SEG_YOLO:
            mask_people = segment_image_YOLO(frame, classes=0)
            mask = cv2.bitwise_not(mask_people)
            mask = cv2.bitwise_and(mask, coarse_mask)
        case _:
            mask = coarse_mask

    # MORPHOLOGY
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(6,6))
    mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)

    # ALPHA MASKING
    alphamask = cv2.GaussianBlur(mask,(7,7),0)
    alphamask = cv2.cvtColor(alphamask, cv2.COLOR_GRAY2BGR)

    return alpha_blending(frame, warped_overlay, alphamask)

    """
    cv2.imshow("coarse_mask", coarse_mask)
    cv2.imshow("closed mask", mask)
    cv2.imshow("alphamask", alphamask)
    cv2.waitKey()
    """

    #NOTE:
    # https://docs.opencv.org/3.4/d0/d86/tutorial_py_image_arithmetics.html
    # https://docs.opencv.org/4.x/df/d9d/tutorial_py_colorspaces.html
    # https://redketchup.io/color-picker


def substitute_LEDscreen_in_img(frame_filename, pattern_filename, overlay_filename, output_filename, 
                                method = OcclusionMethod.SEG_YOLO, n_blocks = 0):
    """Substitutes the LED screen in an image with an overlay image, 
        handling perspective transformation and occlusions.

    Args:
        frame_filename (str): File path of the input frame image.
        pattern_filename (str): File path of the pattern image.
        overlay_filename (str): File path of the overlay image.
        output_filename (str): File path to save the output image.

    """
    frame = cv2.imread(frame_filename)
    pattern = cv2.imread(pattern_filename)
    overlay = cv2.imread(overlay_filename)
  
    frame_markers = detect_in_frame(frame)
    pattern_markers = detect_in_pattern(pattern)
    
    result = process_frame_with_occlusions(frame, pattern, overlay, frame_markers, pattern_markers, method, n_blocks)
    
    cv2.imshow("Image", result)
    cv2.imwrite(output_filename, result)

    cv2.waitKey(0)
    cv2.destroyAllWindows()


def substitute_LEDscreen_in_video(video_file, pattern_file, overlay_file, output_file,
                                  method = OcclusionMethod.SEG_YOLO, n_blocks = 0): #in video:
    """Substitutes the LED screen in each frame of a video with an overlay image, 
        handling perspective transformation and occlusions.

    Args:
        frame_filename (str): File path of the input frame image.
        pattern_filename (str): File path of the pattern image.
        overlay_filename (str): File path of the overlay image.
        output_filename (str): File path to save the output image.

    """
    pattern = cv2.imread(pattern_file)
    overlay = cv2.imread(overlay_file)

    pattern_all_markers = detect_in_pattern(pattern) 

    cap = cv2.VideoCapture(video_file)
    h = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    w = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    fps = int(cap.get(cv2.CAP_PROP_FPS))
    result = cv2.VideoWriter(output_file, cv2.VideoWriter_fourcc(*'MJPG'), fps, (w, h))

    cap.set(cv2.CAP_PROP_FRAME_WIDTH, w)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, h)
    #i = 0
    
    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            break

        #start = time.time()
        curr_frame_markers = detect_in_frame(frame)
        processed_frame = process_frame_with_occlusions(frame, pattern, overlay, curr_frame_markers, pattern_all_markers, 
                                                        method, n_blocks)
        cv2.imshow("Result", processed_frame)
        result.write(processed_frame)
        key = cv2.waitKey(1) & 0xFF
        if key == ord("q"):
                break
        #start = print_time(start, time.time(), "frame " + str(i) + " done in:")
        #i+=1

    
    print("done")
    cv2.destroyAllWindows()
    cap.release()
    result.release()



################### EVALUATION ###################


def print_time(start, end, message):
    print(message, end - start)
    return end


def tests_on_videos(video_file, pattern_file, overlay_file, output_file="temp.avi", fst_frame = 0): #, fst_frame, last_frame):
    """
    Structure for custom test
    """
    pattern = cv2.imread(pattern_file)
    overlay = cv2.imread(overlay_file)

    pattern_all_markers = detect_in_pattern(pattern)

    h = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    w = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    fps = int(cap.get(cv2.CAP_PROP_FPS))
    cap = cv2.VideoCapture(video_file)
    result = cv2.VideoWriter(output_file, cv2.VideoWriter_fourcc(*'MJPG'), fps, (w, h))

    cap.set(cv2.CAP_PROP_FRAME_WIDTH, w)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, h)

    
    cap.set(cv2.CAP_PROP_POS_FRAMES, fst_frame-1)
    i = fst_frame-1
    while cap.isOpened():
            start = time.time()
            ret, frame = cap.read()
            if not ret:
                break
            i+=1
            #if(i % 25 != 0):
            #    continue
            print("frame", i)
            #start = print_time(start, time.time(), "loaded in")
            curr_frame_markers = detect_in_frame(frame)
            #start = print_time(start, time.time(), "detected in")

            #img = process_frame_with_occlusions(frame, pattern, overlay, curr_frame_markers, pattern_all_markers)
            img = get_warped_overlaid_frame(frame, pattern, overlay, curr_frame_markers, pattern_all_markers)

            #start = print_time(start, time.time(), "maskouted in")
            cv2.imshow("video", img)
            result.write(img)
            #print_time(start, time.time(), "saved in")
            #print()

    print("done")
    cv2.destroyAllWindows()
    cap.release()
    result.release()


def aruco_draw(corners, ids, img):
    """Highlights the markers with their IDs of the input image (without changing it).
    
    Returns:
        image: copy of input img with highlighted markers and IDs
    """

    image = img.copy() # not changing the original img

    if len(corners) <= 0: 
        return image
	
    ids = ids.flatten()
    
    for (markerCorner, markerID) in zip(corners, ids):
        corners = markerCorner.reshape((4, 2))
        (topLeft, topRight, bottomRight, bottomLeft) = corners
        
        topRight = (int(topRight[0]), int(topRight[1]))
        bottomRight = (int(bottomRight[0]), int(bottomRight[1]))
        bottomLeft = (int(bottomLeft[0]), int(bottomLeft[1]))
        topLeft = (int(topLeft[0]), int(topLeft[1]))

        cv2.line(image, topLeft, topRight, (0, 255, 0), 2)
        cv2.line(image, topRight, bottomRight, (0, 255, 0), 2)
        cv2.line(image, bottomRight, bottomLeft, (0, 255, 0), 2)
        cv2.line(image, bottomLeft, topLeft, (0, 255, 0), 2)
		
        cX = int((topLeft[0] + bottomRight[0]) / 2.0)
        cY = int((topLeft[1] + bottomRight[1]) / 2.0)
        cv2.circle(image, (cX, cY), 4, (0, 0, 255), -1)
		
        #cv2.putText(image, str(markerID),(topLeft[0], topLeft[1] - 10), cv2.FONT_HERSHEY_SIMPLEX,
        cv2.putText(image, str(markerID),(cX, cY), cv2.FONT_HERSHEY_SIMPLEX,
				0.5, (0, 255, 0), 2)
        #print("ArUco marker ID: {}".format(markerID))

    return image


def aruco_detect_and_display(infile, outfile, aruco_type = "DICT_4X4_100"):
    """Detect and displayes the markers detected in image
        and stores the result into an output file.
    
    Args:
        infile (filepath): input image
        outfile (filepath): output image
        aruco_type (string): key for ARUCO_DICT variable
    
    """
    img = cv2.imread(infile)
    corners, ids = aruco_detect(img, aruco_type)
    result = aruco_draw(corners, ids, img)
    cv2.imwrite(outfile, result)
    cv2.imshow("result", result)
    cv2.waitKey(0)


def aruco_straight_line(corners, img):
    """Draws a straight line on the input image connecting 
        the first and last corners of an ArUco marker.

    Args:
        corners (list): List of ArUco marker corners detected in the image.
        img (ndarray): Input image.

    Returns:
        ndarray: Copy of the input image with a straight line drawn on it.
    """
    image = img.copy() # not changing the original img

    if len(corners) <= 1: 
        return image
    
    cornerA = corners[0][0][0]
    cornerB = corners[-1][0][1]
            
    topLeft = (int(cornerA[0]), int(cornerA[1]))
    topRight = (int(cornerB[0]), int(cornerB[1]))

    print(topLeft, topRight)
    thickness=1
    cv2.line(image, topLeft, topRight, (0, 0, 255), thickness)

    return image


def aruco_draw_corners(corners, img):
    """Highlights the markers corners.
    
    Returns:
        image: copy of input img with highlighted markers and IDs
    """

    image = img.copy() # not changing the original img

    if len(corners) <= 0: 
        return image
	    
    for markerCorner in corners:
        corners = markerCorner.reshape((4, 2))
        (topLeft, topRight, bottomRight, bottomLeft) = corners
        
        topRight = (int(topRight[0]), int(topRight[1]))
        ##bottomRight = (int(bottomRight[0]), int(bottomRight[1]))
        #bottomLeft = (int(bottomLeft[0]), int(bottomLeft[1]))
        topLeft = (int(topLeft[0]), int(topLeft[1]))

        #cv2.circle(image, topLeft, 4, (0, 0, 255), -1)
        #cv2.circle(image, topRight, 4, (0, 0, 255), -1)
        #cv2.circle(image, bottomRight, 4, (0, 0, 255), -1)
        #cv2.circle(image, bottomLeft, 4, (0, 0, 255), -1)
        thickness=1

        cv2.line(image, (topRight[0]-10, topRight[1]-10), (topRight[0]+10, topRight[1]+10), (0, 255, 0), thickness)
        cv2.line(image, (topRight[0]-10, topRight[1]+10), (topRight[0]+10, topRight[1]-10), (0, 255, 0), thickness)

        cv2.line(image, (topLeft[0]-10, topLeft[1]-10), (topLeft[0]+10, topLeft[1]+10), (0, 255, 0), thickness)
        cv2.line(image, (topLeft[0]-10, topLeft[1]+10), (topLeft[0]+10, topLeft[1]-10), (0, 255, 0), thickness)

    return image


def drawUsedFeatures(input, corners, filename):
    """Draw the corners into (a copy of) input image and displays/stores the result."""
    colors = [(0, 0, 255), (0, 255, 0), (255, 0, 0),
                (0, 255, 255), (255, 0, 255), (255, 255, 0),
                (255, 255, 255), (0, 0, 0)]
    i = 0
    output = input.copy()
    for corner in corners:
        cv2.circle(output, (int(corner[0]), int(corner[1])), 10, colors[i%len(colors)], -1)
        i+=1

    cv2.imshow("corners", output)
    cv2.imwrite(filename, output)
    cv2.waitKey(0)


def show_img_histograms(path):
    """Reads an image from the given path, calculates and displays the color histograms
        for the RGB and HSV channels.

    Args:
        path (str): Path to the image file.

    Returns:
        tuple: Tuple containing the histograms for the RGB and HSV channels 
            (hist_r, hist_g, hist_b, hist_h, hist_s, hist_v).
    """
    img = cv2.imread(path)
    b, g, r = img[:,:,0], img[:,:,1], img[:,:,2]
    hist_b = cv2.calcHist([b],[0],None,[256],[0,256])
    hist_g = cv2.calcHist([g],[0],None,[256],[0,256])
    hist_r = cv2.calcHist([r],[0],None,[256],[0,256])
    plt.plot(hist_r, color='r', label="r")
    plt.plot(hist_g, color='g', label="g")
    plt.plot(hist_b, color='b', label="b")
    plt.legend()
    plt.show() 
    img2 = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    h, s, v = img2[:,:,0], img2[:,:,1], img2[:,:,2]
    hist_h = cv2.calcHist([h],[0],None,[256],[0,256])
    hist_s = cv2.calcHist([s],[0],None,[256],[0,256])
    hist_v = cv2.calcHist([v],[0],None,[256],[0,256])
    plt.plot(hist_h, color='r', label="h")
    plt.plot(hist_s, color='g', label="s")
    plt.plot(hist_v, color='b', label="v")
    plt.legend()
    plt.show()
    
    return hist_r,hist_g, hist_b, hist_h, hist_s, hist_v


def combine_four_videos(video_paths, output_width, output_height, output_filename, labels, fps = 50):
    """Combines four videos into a single video, arranging them in a 2x2 grid pattern.

    Args:
        video_paths (list): List of paths to the input video files.
        output_width (int): Width of the output video frame.
        output_height (int): Height of the output video frame.
        output_filename (str): Filename of the output video file.
        labels (list): List of labels to be displayed on the combined video frame.
        fps (int, optional): Frames per second for the output video. Default is 50.
    """
    cap_list = []
    frame_list = []

    # Open video capture for each video file
    for path in video_paths:
        cap = cv2.VideoCapture(path)
        cap_list.append(cap)
        ret, frame = cap.read()
        frame = cv2.resize(frame, (output_width // 2, output_height // 2))  # Resize frames to common size
        frame_list.append(frame)

    # Create a VideoWriter object to save the final video
    fourcc = cv2.VideoWriter_fourcc(*"mp4v")  # Specify the codec
    output_video = cv2.VideoWriter(output_filename, fourcc, fps, (output_width, output_height))

    while True:
        combined_frame = np.zeros((output_height, output_width, 3), dtype=np.uint8)

        # Arrange the frames in quarters of the output frame
        combined_frame[:output_height // 2, :output_width // 2] = frame_list[0]
        combined_frame[:output_height // 2, output_width // 2:] = frame_list[1]
        combined_frame[output_height // 2:, :output_width // 2] = frame_list[2]
        combined_frame[output_height // 2:, output_width // 2:] = frame_list[3]

        # Add file names as text in the combined frame
        cv2.putText(combined_frame, labels[0], (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2, cv2.LINE_AA)
        cv2.putText(combined_frame, labels[1], (output_width // 2 + 10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2, cv2.LINE_AA)
        cv2.putText(combined_frame, labels[2], (10, output_height // 2 + 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2, cv2.LINE_AA)
        cv2.putText(combined_frame, labels[3], (output_width // 2 + 10, output_height // 2 + 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2, cv2.LINE_AA)


        cv2.imshow("Combined Video", combined_frame)

        # Write the combined frame to the output video
        output_video.write(combined_frame)

        key = cv2.waitKey(1)
        if key == 27 or key == ord('q') or not ret:  # Press Esc / q key to exit
            break

        # Read the next frame for each video
        for i in range(len(cap_list)):
            ret, frame = cap_list[i].read()
            if ret:
                frame = cv2.resize(frame, (output_width // 2, output_height // 2))  # Resize frames to common size
                frame_list[i] = frame

    # Release video captures, close windows, and release the output video
    for cap in cap_list:
        cap.release()
    cv2.destroyAllWindows()
    output_video.release()


######################################################

def main():
    
    """Example usage:"""
    substitute_LEDscreen_in_img("./input_files/frames/clip0-frame.png", "./input_files/patterns/panel00-crop.png", "./input_files/adds/add.png", "temp.png", OcclusionMethod.SEG_YOLO)
    #substitute_LEDscreen_in_img("./input_files/frames/clip0-frame.png", "./input_files/patterns/panel00-crop.png", "./input_files/adds/add.png", "temp.png", OcclusionMethod.CHROMA_KEY, n_blocks=7)

    #substitute_LEDscreen_in_video("./input_files/videos/clip2-white.mp4", "./input_files/patterns/pattern02-white.png", "./input_files/adds/add.png", "white-diffkey.avi", OcclusionMethod.DIFF_KEY)
    #substitute_LEDscreen_in_video("./input_files/videos/clip2-white.mp4", "./input_files/patterns/pattern02-white.png", "./input_files/adds/add.png", "white-chromakey.avi", OcclusionMethod.CHROMA_KEY)
    #substitute_LEDscreen_in_video("./input_files/videos/clip2-white.mp4", "./input_files/patterns/pattern02-white.png", "./input_files/adds/add.png", "white-segyolo.avi", OcclusionMethod.SEG_YOLO)
    pass

    #NOTE: q-key closes window


if __name__ == "__main__":
    main()

